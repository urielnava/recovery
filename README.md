# README #
Este script funciona con las tres fases de un ETL convencional,para recuperar la informacion faltante del command center.

## Run Script ##
Para ejecutar el script es necesario ejecutarlo en python 2.7

## Paquetes ##
pip install mysql-connector-python
pip install more-itertools
pip install unicodecsv
pip install unicodedata2

## Extracion ##
Este script se conecta a cuatro bases de datos las cuales son sales_production,travels_production,paybus_producction y cinnamon_production.
Durante la fase de extraccion se ejecuntan 5 querys principales los cuales obtienen los id de las tablas clave,las cuales son sales_id,reservation_id y bus reservation.

## Transformacion ##
Una vez que se tienen los ids, se ejecutan querys de proyeccion, sobres los campos de interes, se forma una matriz rectangular y se formatean campos como los de fechas o los de precios.

## Carga ##
Durante la fase de carga se crea un archivo csv el cual contiene la matriz de datos generados.