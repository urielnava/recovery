# encoding=utf8

import mysql.connector
import itertools
#import unicodecsv as csv
import unicodedata
import csv

def tupleToArray(data):
    result = list(itertools.chain.from_iterable(data))
    return result

connection_ventas = mysql.connector.connect(
    host     = "sales-db-production-cluster.cav8via6jih7.us-east-1.rds.amazonaws.com",
    user     = "sales",
    password = "jFGJgpH18VU4",
    database = "sales_production"
)

connection_travels = mysql.connector.connect(
    host     = "travels-db-production-cluster.cav8via6jih7.us-east-1.rds.amazonaws.com",
    user     = "travels",
    password = "9K9f4JqXMxwv",
    database = "travels_production"
)

connection_command_center = mysql.connector.connect(
    host = "cinnamon-warehouse.eb.digital",
    user = "command-center",
    password = "29*mJRGhhQ",
    database = "cinnamon_production"
)

connection_paybus = mysql.connector.connect(
    host = "paybus-db-production-cluster.cav8via6jih7.us-east-1.rds.amazonaws.com",
    user = "paybus",
    password = "6!-4?3UzP@gKsWbN",
    database= "paybus_production"
)

first_query = """
SELECT sales_production.sale.id AS "sale_id"
FROM sales_production.sale,sales_production.reservation,sales_production.bus_reservation
WHERE sales_production.sale.created_at > '2020-06-07 05:00:01'
AND  sales_production.sale.created_at < '2020-06-08 04:59:59'
AND sales_production.sale.id = sales_production.reservation.sale_id
AND sales_production.reservation.id = sales_production.bus_reservation.id
AND sales_production.sale.status = "confirmed"
ORDER BY sales_production.sale.created_at DESC
"""

second_query = """
SELECT sales_production.reservation.id AS "sale_reservation_id"
FROM sales_production.sale,sales_production.reservation,sales_production.bus_reservation
WHERE sales_production.sale.created_at > '2020-06-07 05:00:01'
AND  sales_production.sale.created_at < '2020-06-08 04:59:59'
AND sales_production.sale.id = sales_production.reservation.sale_id
AND sales_production.reservation.id = sales_production.bus_reservation.id
AND sales_production.sale.status = "confirmed"
ORDER BY sales_production.sale.created_at DESC
"""

thirt_query = """
SELECT sales_production.bus_reservation.id AS "sale_bus_reservation_id"
FROM sales_production.sale,sales_production.reservation,sales_production.bus_reservation
WHERE sales_production.sale.created_at > '2020-06-07 05:00:01'
AND  sales_production.sale.created_at < '2020-06-08 04:59:59'
AND sales_production.sale.id = sales_production.reservation.sale_id
AND sales_production.reservation.id = sales_production.bus_reservation.id
AND sales_production.sale.status = "confirmed"
ORDER BY sales_production.sale.created_at DESC
"""

four_query ="""
SELECT sales_production.bus_reservation.travel_id AS "travel_id"
FROM sales_production.sale,sales_production.reservation,sales_production.bus_reservation
WHERE sales_production.sale.created_at > '2020-06-07 05:00:01'
AND  sales_production.sale.created_at < '2020-06-08 04:59:59'
AND sales_production.sale.id = sales_production.reservation.sale_id
AND sales_production.reservation.id = sales_production.bus_reservation.id
AND sales_production.sale.status = "confirmed"
ORDER BY sales_production.sale.created_at DESC
"""

five_query = """
SELECT sales_production.sale.charge_id AS "charge_id"
FROM sales_production.sale,sales_production.reservation,sales_production.bus_reservation
WHERE sales_production.sale.created_at > '2020-06-07 05:00:01'
AND  sales_production.sale.created_at < '2020-06-08 04:59:59'
AND sales_production.sale.id = sales_production.reservation.sale_id
AND sales_production.reservation.id = sales_production.bus_reservation.id
AND sales_production.sale.status = "confirmed"
ORDER BY sales_production.sale.created_at DESC
"""
#SELECT domain,DATE_FORMAT(sales_production.sale.created_at, '%Y/%M/%D') AS "Fecha Venta",DATE_FORMAT(sales_production.sale.created_at, '%H:%S:%I') AS "Hora Venta" FROM sales_production.sale WHERE sales_production.sale.id = %s
domain_query = """
SELECT domain,DATE_FORMAT(CONVERT_TZ(sales_production.sale.created_at, 'UTC', 'Mexico/General'),'%Y/%M/%D'),DATE_FORMAT(CONVERT_TZ(sales_production.sale.created_at, 'UTC', 'Mexico/General'),'%H:%S:%I')
FROM sales_production.sale 
WHERE sales_production.sale.id = %s
"""

corrida_query = """
SELECT number,DATE_FORMAT(CONVERT_TZ(travels_production.travel.departure_time, 'UTC', 'Mexico/General'),'%Y/%M/%D'),DATE_FORMAT(CONVERT_TZ(travels_production.travel.departure_time, 'UTC', 'Mexico/General'),'%H:%S:%I')
FROM travels_production.travel
WHERE travels_production.travel.id = %s
"""

route_id_query = """
SELECT route_id FROM travels_production.travel WHERE travels_production.travel.id = %s
"""

origin_id_query = """
SELECT origin_id FROM travels_production.route WHERE travels_production.route.id = %s
"""

destination_id_query = """
SELECT destination_id FROM travels_production.route WHERE travels_production.route.id = %s
"""

origin_geb_query = """
SELECT geb_code FROM cinnamon_production.places_station WHERE id = %s
"""

destiny_geb_query = """
SELECT geb_code FROM cinnamon_production.places_station WHERE id = %s
"""

company_id_query = """
SELECT company_id FROM travels_production.travel WHERE travels_production.travel.id = %s
"""

company_line_id_query = """
SELECT company_line_id FROM travels_production.travel WHERE travels_production.travel.id = %s
"""

company_param_query = """
SELECT param FROM cinnamon_production.companies_company WHERE cinnamon_production.companies_company.id = %s
"""

service_name_query = """
SELECT name FROM cinnamon_production.companies_company_line WHERE company_id = %s
"""

ventas_original_query = """
SELECT (subtotal * 0.01) AS sub,(iva * 0.01) AS iva,((subtotal * 0.01) + (iva * 0.01)) AS Total FROM sales_production.sale WHERE sales_production.sale.id = %s
"""

coupon_query = """
SELECT coupon_id FROM sales_production.sale WHERE sales_production.sale.id = %s
"""

numero_boletos_query = """
SELECT COUNT(*) from sales_production.ticket WHERE bus_reservation_id = %s GROUP BY bus_reservation_id
"""

numero_asiento_query = """
SELECT seat_number,type from sales_production.ticket WHERE bus_reservation_id = %s
"""

folio_operacion_query = """
SELECT folio from sales_production.bus_reservation WHERE id = %s
"""

folio_ticket_query = """
SELECT folio from sales_production.ticket WHERE bus_reservation_id = %s
"""

payment_name_query = """
SELECT name FROM paybus_production.charges,paybus_production.payment_processors WHERE paybus_production.charges.payment_processor_id = paybus_production.payment_processors.id AND paybus_production.charges.id = %s
"""

external_id_query = """
SELECT external_id FROM paybus_production.charges WHERE id = %s
"""

cursor = connection_ventas.cursor()
cursor_travels = connection_travels.cursor()
cursor_command = connection_command_center.cursor()
cursor_paybus = connection_paybus.cursor()

cursor.execute(first_query)
sales_id = cursor.fetchall()
cursor.execute(second_query)
sales_reservation_id = cursor.fetchall()

cursor.execute(thirt_query)
sales_bus_reservation_id = cursor.fetchall()

cursor.execute(four_query)
sales_bus_reservation_travel_id = cursor.fetchall()

cursor.execute(five_query)
charges_id = cursor.fetchall()


sales_id = tupleToArray(sales_id)
sales_reservation_id = tupleToArray(sales_reservation_id)
sales_bus_reservation_id = tupleToArray(sales_bus_reservation_id)
sales_bus_reservation_travel_id = tupleToArray(sales_bus_reservation_travel_id)
charges_id = tupleToArray(charges_id)


domain = []
corrida = []
route_id = []
origin_id = []
destination_id = []
origin_geb = []
destination_geb = []
company_id = []
company_line_id = []
company_param = []
service_name = []
ventas_original = []
cupones = []
numero_boletos = []
numero_asiento = []
folio_operacion = []
folio_ticket = []
external_id = []
payment_name = []


for r in sales_id:
    a = (r,)
    cursor.execute(domain_query,a)
    temporal = cursor.fetchall()
    domain.append(temporal[0])

for r in sales_bus_reservation_travel_id:
    a = (r,)
    cursor_travels.execute(corrida_query,a)
    temporal = cursor_travels.fetchall()
    corrida.append(temporal[0])

for r in sales_bus_reservation_travel_id:
    a = (r,)
    cursor_travels.execute(route_id_query,a)
    temporal = cursor_travels.fetchall()
    route_id.append(temporal[0])

for r in route_id:
    a = (r[0],)
    cursor_travels.execute(origin_id_query,a)
    temporal = cursor_travels.fetchall()
    origin_id.append(temporal[0])

for r in route_id:
    a = (r[0],)
    cursor_travels.execute(destination_id_query,a)
    temporal = cursor_travels.fetchall()
    destination_id.append(temporal[0])

for r in origin_id:
    a = (r[0],)
    cursor_command.execute(origin_geb_query,a)
    temporal = cursor_command.fetchall()
    origin_geb.append(temporal[0])

for r in destination_id:
    a = (r[0],)
    cursor_command.execute(destiny_geb_query,a)
    temporal = cursor_command.fetchall()
    destination_geb.append(temporal[0])

for r in sales_bus_reservation_travel_id:
    a = (r,)
    cursor_travels.execute(company_id_query,a)
    temporal = cursor_travels.fetchall()
    company_id.append(temporal[0])

for r in sales_bus_reservation_travel_id:
    a = (r,)
    cursor_travels.execute(company_line_id_query,a)
    temporal = cursor_travels.fetchall()
    company_line_id.append(temporal[0])

for r in company_id:
    a = (r[0],)
    cursor_command.execute(company_param_query,a)
    temporal = cursor_command.fetchall()
    company_param.append(temporal[0])

for r in company_id:
    a = (r[0],)
    cursor_command.execute(service_name_query,a)
    temporal = cursor_command.fetchall()
    service_name.append(temporal[0])

for r in sales_id:
    a = (r,)
    cursor.execute(ventas_original_query,a)
    temporal = cursor.fetchall()
    ventas_original.append(temporal)

for r in sales_id:
    a = (r,)
    cursor.execute(coupon_query,a)
    temporal = cursor.fetchall()
    cupones.append(temporal)

for r in sales_bus_reservation_id:
    a = (r,)
    cursor.execute(numero_boletos_query,a)
    temporal = cursor.fetchall()
    numero_boletos.append(temporal)

for r in sales_bus_reservation_id:
    a = (r,)
    cursor.execute(numero_asiento_query,a)
    temporal = cursor.fetchall()
    numero_asiento.append(temporal)

for r in sales_bus_reservation_id:
    a = (r,)
    cursor.execute(folio_operacion_query,a)
    temporal = cursor.fetchall()
    folio_operacion.append(temporal)

for r in sales_bus_reservation_id:
    a = (r,)
    cursor.execute(folio_ticket_query,a)
    temporal = cursor.fetchall()
    folio_ticket.append(temporal)

for r in charges_id:
    a = (r,)
    cursor_paybus.execute(payment_name_query,a)
    temporal = cursor_paybus.fetchall()
    payment_name.append(temporal)

for r in charges_id:
    a = (r,)
    cursor_paybus.execute(external_id_query,a)
    temporal = cursor_paybus.fetchall()
    external_id.append(temporal)

headers = ["Domain","Fecha Venta","Hora Venta","No. Corrida","Fecha Salida","Hora Salida","Origen","Destino","Empresa","Venta Subtotal Original","Venta Iva Original","Venta Total","Cupon","No. Boletos","Asiento","Tipo Boleto","Folio Operacion","Folio Boleto","Conekta ID","External Name"]

print('Starting data building ...')

data = []

data.append(headers)

for p in domain:
    data_row = []
    data_row.append(p[0])
    data_row.append(p[1])
    data_row.append(p[2])
    data.append(data_row)

print('domain data build ...')

index = 1
for p in corrida:
    data[index].append(p[0])
    data[index].append(p[1])
    data[index].append(p[2])
    index = index + 1

print('corrida data build ...')

index = 1
for p in origin_geb:
    data[index].append(p[0])
    index = index + 1

index = 1
for p in destination_geb:
    data[index].append(p[0])
    index = index + 1

index = 1
for p in company_param:
    data[index].append(p[0])
    index = index + 1

print('company param data build ...')


index = 1
for p in ventas_original:
    data[index].append(p[0][0])
    data[index].append(p[0][1])
    data[index].append(p[0][2])
    index = index + 1
print('decimales data build')

index = 1
for p in cupones:
    if len(data) > index:
        if p[0][0] == None:
            data[index].append("")
        else:
            data[index].append(p[0][0])
    index = index + 1
print('cupones data build ...')

index = 1
for p in numero_boletos:
    data[index].append(p[0][0])
    index = index + 1

print('boletos data build ...')

index = 1
for p in numero_asiento:
    data[index].append(p[0][0])
    data[index].append(p[0][1])
    index = index + 1
print('asientos data build ...')

index = 1
for p in folio_operacion:    
    if p == None:
        data[index].append("")
    else:
        data[index].append(p[0][0])
    index = index + 1
print('folio operacion data build ...')

index = 1
for p in folio_ticket:
    data[index].append(p[0][0])
    index = index + 1
print('folio ticket data build ...')


index = 1
for p in external_id:
    if p != []:
        if p[0][0] == None:
            data[index].append("")
        else:
            data[index].append(p[0][0])
    index = index + 1
print('external ID data build ...')

index = 1
for p in payment_name:
    data[index].append(p[0][0])
    index = index + 1
print('payment name data build ...')

with open('recovery_2020-06-07.csv', 'wb') as myfile:
    wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
    for row in data:
        wr.writerow(row)
        
print("CSV was created")